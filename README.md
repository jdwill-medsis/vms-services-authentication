# README #

### UAA OAuth2 Authentication Server ###

This is a modified version of Pivotal's UAA server.
https://github.com/cloudfoundry/uaa

### Setup ###

Navigate to the root directory and enter the following command:
$ ./gradlew run --info

Note: The server will appear to stall with the console message "Building 97%."  
If the message "Press Ctrl-C to stop the container...", the server is running normally.# README #

### UAA OAuth2 Authentication Server ###

This is a modified version of Pivotal's UAA server.
https://github.com/cloudfoundry/uaa
